﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCK_2.Models;
using KCK_2.DBAL;
using KCK_2.ViewModel;
using System.Web.Security;
using System.Diagnostics;

namespace KCK_2.Controllers
{
    public class HomeController : Controller
    {   
        [Authorize]
        public ActionResult AddCar()
        {            
            DataBase dataBase = new DataBase();
            ViewBag.list = dataBase.GetCarDealersList();
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddCar(AddEditVM car)
        {
            DataBase dataBase = new DataBase();

            if (ModelState.IsValid)
            {               
                dataBase.AddCar(dataBase.GetIdUser(car.LoggedUser), car.IdCarDealer, car.MakeOfACar, car.Model, car.Prize);
                return RedirectToAction("Menu");
            }
            else
            {
                ViewBag.list = dataBase.GetCarDealersList();
                return View("AddCar", car);
            }
        }

        [Authorize]
        public ActionResult RemoveCar(int id)
        {
            DataBase dataBase = new DataBase();
            dataBase.RemoveCar(id);
            return RedirectToAction("Menu");
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("FirstMenu", "Home");
        }

        [Authorize]
        public ActionResult EditCar(int id)
        {
            DataBase dataBase = new DataBase();
            ViewBag.list = dataBase.GetCarDealersList();

            AddEditVM addEditVM = new AddEditVM();

            foreach(var item in dataBase.GetCarsList())
            {
                if (item.IdNotice == id)
                {
                    addEditVM.IdNotice = item.IdNotice;
                    addEditVM.IdCarDealer = item.IdCarDealer;
                    addEditVM.IdAccount = item.IdAccount;
                    addEditVM.MakeOfACar = item.MakeOfACar;
                    addEditVM.Model = item.Model;
                    addEditVM.Prize = item.Prize;
                }
            }
            return View(addEditVM);
        }

        [HttpPost]
        public ActionResult EditCar(AddEditVM car)
        {
            DataBase dataBase = new DataBase();

            if (ModelState.IsValid)
            {
                dataBase.EditCar(car.IdNotice, car.IdCarDealer, car.MakeOfACar, car.Model, car.Prize);
                return RedirectToAction("Menu");
            }
            else
            {
                ViewBag.list = dataBase.GetCarDealersList();
                return View("EditCar", car);
            }
        }

        public ActionResult FirstMenu()
        {
            return View();
        }

        [Authorize]
        public ActionResult Menu()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Account account)
        {
            DataBase dataBase = new DataBase();

            if (ModelState.IsValid)
            {
                if (dataBase.IsFreeUser(account.Username))
                {
                    dataBase.AddAccount(account.Username, account.Password, account.Firstname, account.Number);

                    return RedirectToAction("Login");
                }
                else
                {
                    ModelState.AddModelError("CredentialError", "Nazwa użytkownika jest zajęta");
                }
                return View("Register");
            }
            else
            {
                return View("Register", account);
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                DataBase dataBase = new DataBase();

                if (dataBase.IsValidUser(username, password))
                {
                    FormsAuthentication.SetAuthCookie(username, false);
                    if (ReturnUrl == null)
                    {                        
                        return RedirectToAction("Menu", "Home");
                    }
                    return Redirect(ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("CredentialError", "Bledne haslo/nazwa uzytkownika");
                    return View("Login");
                }
            }
            else
            {
                LoginVM loginVM = new LoginVM();
                loginVM.Username = username;
                loginVM.Password = password;
                return View("Login", loginVM);
            }
        }

        [Authorize]
        public ActionResult ShowCarLoggin()
        {
            DataBase dataBase = new DataBase();
            ShowListVM showListVM = new ShowListVM();
            showListVM.LogoutShowVMs = TransferAll(dataBase.GetCarDealersList(), dataBase.GetCarsList(), dataBase.GetLoginList());
            return View(showListVM);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ShowCarLoggin(ShowListVM showListAfter)
        {
            DataBase dataBase = new DataBase();
            ShowListVM showListVM = new ShowListVM();
            if (showListAfter.Text != null && showListAfter.Text.Trim().Length > 0)
            {
                showListVM.LogoutShowVMs = TransferAllName(dataBase.GetCarDealersList(), dataBase.GetCarsList(), dataBase.GetLoginList(), showListAfter.Text);
                showListAfter.Text = null;
                return View(showListVM);
            }
            else return RedirectToAction("ShowCarLoggin");            
        }

        public ActionResult ShowCar()
        {   
            DataBase dataBase = new DataBase();
            ShowListVM showListVM = new ShowListVM();
            showListVM.LogoutShowVMs = TransferAll(dataBase.GetCarDealersList(), dataBase.GetCarsList(), dataBase.GetLoginList());
            return View(showListVM);
        }

        [HttpPost]
        public ActionResult ShowCar(ShowListVM showListAfter)
        {
            DataBase dataBase = new DataBase();
            ShowListVM showListVM = new ShowListVM();
            if (showListAfter.Text != null && showListAfter.Text.Trim().Length > 0)
            {
                showListVM.LogoutShowVMs = TransferAllName(dataBase.GetCarDealersList(), dataBase.GetCarsList(), dataBase.GetLoginList(), showListAfter.Text);
                showListAfter.Text = null;
                return View(showListVM);
            }
            return RedirectToAction("ShowCar");
        }

        public static List<LogoutShowVM> TransferAll(List<CarDealer> listDealer, List<DetailCar> listDetailCars, List<Account> listAccount)
        {
            List<LogoutShowVM> logoutShowVMs = new List<LogoutShowVM>();

            foreach (var x in listDetailCars)
            {       
                LogoutShowVM login = new LogoutShowVM();
                
                login.Id_notice = x.IdNotice;
                login.MakeOfACar = x.MakeOfACar;
                login.Model = x.Model;
                login.Prize = x.Prize;

                CarDealer dealer = listDealer.Where(u => u.IDCarDealer == x.IdCarDealer).Single();
                Account account = listAccount.Where(u => u.IDAccount == x.IdAccount).Single();

                login.Name = dealer.Name;
                login.Street = dealer.Street;

                login.Username = account.Username;
                login.Firstname = account.Firstname;
                login.Number = account.Number;

                logoutShowVMs.Add(login);
            }
            return logoutShowVMs;
        }

        public static List<LogoutShowVM> TransferAllName(List<CarDealer> listDealer, List<DetailCar> listDetailCars, List<Account> listAccount, string name)
        {
            List<LogoutShowVM> logoutShowVMs = new List<LogoutShowVM>();

            name = name.ToLower();

            foreach (var x in listDetailCars)
            {
                if (x.MakeOfACar.ToLower().StartsWith(name))
                {
                    LogoutShowVM login = new LogoutShowVM();

                    login.Id_notice = x.IdNotice;
                    login.MakeOfACar = x.MakeOfACar;
                    login.Model = x.Model;
                    login.Prize = x.Prize;
                    

                    CarDealer dealer = listDealer.Where(u => u.IDCarDealer == x.IdCarDealer).Single();
                    Account account = listAccount.Where(u => u.IDAccount == x.IdAccount).Single();

                    login.Name = dealer.Name;
                    login.Street = dealer.Street;

                    login.Username = account.Username;
                    login.Firstname = account.Firstname;
                    login.Number = account.Number;

                    logoutShowVMs.Add(login);
                }
            }
            return logoutShowVMs;
        }

        public void Swap()
        {

        }
    }
}
