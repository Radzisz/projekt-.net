﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Data.Entity;
using KCK_2.Models;

namespace KCK_2.DBAL
{
    class DataBase : DbContext
    {

        public DataBase() : base("KCKZadanie2")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().ToTable("table.Account");
            modelBuilder.Entity<DetailCar>().ToTable("table.DetailCar");
            modelBuilder.Entity<CarDealer>().ToTable("table.CarDealer");


            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Account> BazaAccountDB { get; set; }
        public DbSet<DetailCar> BazaDetailCarDB { get; set; }
        public DbSet<CarDealer> BazaCarDealersDB { get; set; }

        public void Start()
        {
            List<CarDealer> carDealers = BazaCarDealersDB.ToList();

            if (carDealers.Count() < 1)
            {


                BazaCarDealersDB.Add(new CarDealer { Name = "Top Auto", Street = "Krupniki 6" });
                BazaCarDealersDB.Add(new CarDealer { Name = "ASW", Street = "Maczka 51" });
                BazaCarDealersDB.Add(new CarDealer { Name = "Top Motors", Street = "Porosły 1F" });
                BazaCarDealersDB.Add(new CarDealer { Name = "Sieńko i Syn", Street = "Wysockiego 65" });
                SaveChanges();
            }
        }

        public bool IsValidUser(string username, string password)
        {
            foreach (var d in BazaAccountDB)
            {
                if (d.Username == username && SHA1Hash(password + "mysalt1f$gO3123qs") == d.Password)
                {
                    return true;
                }

            }
            return false;
        }

        public void AddAccount(string username, string password, string firstname, string number)
        {

            string password2 = SHA1Hash(password + "mysalt1f$gO3123qs");
            BazaAccountDB.Add(new Account { Username = username, Password = password2, Firstname = firstname, Number = number });
            SaveChanges();
        }

        public void AddCar(int idAccount, int idCarDealer, string makeofacar, string model, string prize)
        {
            BazaDetailCarDB.Add(new DetailCar { IdAccount = idAccount, MakeOfACar = makeofacar, Model = model, Prize = prize, IdCarDealer = idCarDealer });

            SaveChanges();
        }

        public void EditCar(int idNotice, int idCarDealer, string makeofacar, string model, string prize)
        {


            foreach (DetailCar car in BazaDetailCarDB)
            {
                if (car.IdNotice == idNotice)
                {
                    car.MakeOfACar = makeofacar;
                    car.Model = model;
                    car.Prize = prize;
                    car.IdCarDealer = idCarDealer;
                }
            }
            SaveChanges();
        }

        public void RemoveCar(int id)
        {
            DetailCar detailCar = new DetailCar();
            detailCar = BazaDetailCarDB.Where(u => u.IdNotice == id).Single();
            BazaDetailCarDB.Remove(detailCar);
            SaveChanges();
        }

        public List<DetailCar> GetCarsList()
        {
            return BazaDetailCarDB.ToList();
        }

        public List<CarDealer> GetCarDealersList()
        {
            return BazaCarDealersDB.ToList();
        }

        public List<Account> GetLoginList()
        {
            return BazaAccountDB.ToList();
        }

        public string GetUsername(int index)
        {
            string username = " ";

            foreach (var d in BazaAccountDB)
            {
                if (d.IDAccount == index)
                {
                    username = d.Username;
                }
            }
            return username;
        }


        public int GetIdUser(string username)
        {
            int Id = 0;

            foreach (var d in BazaAccountDB)
            {
                if (d.Username == username)
                {

                    Id = d.IDAccount;
                }

            }

            return Id;
        }



        public bool IsFreeUser(string username)
        {
            foreach (var d in BazaAccountDB)
            {
                if (d.Username == username)
                {

                    return false;
                }

            }
            return true;
        }



        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2"); sb.Append(hex);
            }
            return sb.ToString();
        }

        public static string SHA1Hash(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            return HexStringFromBytes(hashBytes);
        }
    }
}