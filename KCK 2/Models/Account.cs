﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.Models
{
    public class Account 
    {
        [Key]
        public int IDAccount { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Login")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Imie")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Numer telefonu")]
        [RegularExpression(@"^([0-9]{9})$", ErrorMessage = "Zły format numeru.")]
        public string Number { get; set; }
    }
}