﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.Models
{
    public class CarDealer
    {
        [Key]
        public int IDCarDealer { get; set; }

        [Required]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Ulica")]
        public string Street { get; set; }
    }
}