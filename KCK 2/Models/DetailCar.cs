﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.Models
{
    public class DetailCar
    {
        [Key]
        public int IdNotice { get; set; }

        [Required]
        [Display(Name = "Id Salonu")]
        public int IdCarDealer { get; set; }

        [Required]
        [Display(Name = "Id Konta")]
        public int IdAccount { get; set; }

        [Required]
        [Display(Name = "Marka")]
        public string MakeOfACar { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required]
        [Display(Name = "Cena")]
        public string Prize { get; set; }
    }
}