﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCK_2.Models;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.ViewModel
{
    public class AddEditVM 
    {
        [Key]
        public int IdNotice { get; set; }

        [Required]
        [Display(Name = "Id Salonu")]
        public int IdCarDealer { get; set; }

        [Required]
        [Display(Name = "Id Konta")]
        public int IdAccount { get; set; }

        [Required]
        [Display(Name = "Marka")]
        public string MakeOfACar { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required]
        [Display(Name = "Cena")]
        [RegularExpression("[0-9 ]*$", ErrorMessage = "Podana cena jest nieprawidłowa")]
        public string Prize { get; set; }

        public string LoggedUser { get; set; }

        public AddEditVM()
        {
            LoggedUser = HttpContext.Current.User.Identity.Name;
        }
    }
}