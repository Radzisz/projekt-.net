﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.ViewModel
{
    public class LoginShowVM
    {
        [Key]
        public int Id_notice { get; set; }

        [Required]
        [Display(Name = "Marka")]
        public string MakeOfACar { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required]
        [Display(Name = "Cena")]
        public string Prize { get; set; }

        [Required]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Ulica")]
        public string Street { get; set; }
    }
}