﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.ViewModel
{
    public class LoginVM
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Nazwa uzytkownika")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Haslo")]
        public string Password { get; set; }

        public string LoggedUser { get; set; }

        public LoginVM()
        {
            LoggedUser = HttpContext.Current.User.Identity.Name;
        }
    }
}