﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KCK_2.ViewModel
{
    public class LogoutShowVM
    {
        [Key]
        public int Id_notice { get; set; }

        [Required]
        [Display(Name = "Marka")]
        public string MakeOfACar { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string Model { get; set; }

        [Required]
        [Display(Name = "Cena")]
        public string Prize { get; set; }

        [Required]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "ID właściciela")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Imie")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Numer telefonu")]
        [Phone]
        public string Number { get; set; }
    }
}