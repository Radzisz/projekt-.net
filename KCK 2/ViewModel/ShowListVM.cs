﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KCK_2.ViewModel
{
    public class ShowListVM 
    {
        public List<LogoutShowVM> LogoutShowVMs { get; set; }
        public String Text { get; set; }
        public string LoggedUser { get; set; }

        public ShowListVM()
        {
            LoggedUser = HttpContext.Current.User.Identity.Name;
        }
    }

    
}